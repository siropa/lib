# Download and Install SIROPA Maple archive

- Download the `siropa.mla` file in `[SOME DIRECTORY]`
- launch Maple and then run
```
libname := "[SOME DIRECTORY]/siropa.mla", libname;
with(Siropa);
```
- All the functions are documented [here](https://siropa.gitlabpages.inria.fr) - you can alternatively clone/download the html directory to get a local copy of the documentation.

